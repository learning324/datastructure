function sumOfArray(n) {
    let sum = 0; //1

    for(let el of n) { //1
        sum = sum + el; //n
    }
    return sum; //1
    // return n.reduce((sum, curentNumber) => sum + curentNumber, 0 )
}

sumOfArray([1, 2, 3, 4, 5])
// Time complexity O(n)