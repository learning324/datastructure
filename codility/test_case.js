function solution(T, R) {
    const res = {};
    let ree = [];
    const groups = new Map();
    let r;
    let sol;

    for(let index = 0; index < T.length; index++){
        res[T[index]] = R[index];
    }
    
    for(const el of T) {
        let a = el.split("");
        if(isNaN(a.splice(-1)[0])) {
            r = el.substring(0, el.length - 1)
            groups.set(r)         
        }else{
            groups.set(el)
        }
    }

    console.log(res);
    console.log(groups);



}

solution(
    ["test1a", "test2", "test1b", "test1c", "test3"], 
    ["Wromg answer", "OK", "OK", "Runtime error", "OK"]
);