const ids = new Set([]);

ids.add('abc');
ids.add(1);
ids.add('bb2');
ids.add(1);

console.log(ids);

for(const el of ids) {
    console.log(el);
}

console.log(ids.has(1));
console.log(ids.delete('abc'));
console.log(ids);
