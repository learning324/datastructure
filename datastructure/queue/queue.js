class Queue {
    constructor() {
        this.items = [];
    }

    enQueue(value) {
        this.items.unshift(value)
    }

    deQueue() {
        return this.items.pop();
    }

    isEmpty() {
        return this.items.length === 0;
    }

    toArray() {
        return this.items.slice();
    }
}

const queue = new Queue();

queue.enQueue('max');
queue.enQueue('apoena');
queue.enQueue('joshua');
console.log(queue.toArray());
console.log(queue.deQueue());
console.log(queue.toArray());
