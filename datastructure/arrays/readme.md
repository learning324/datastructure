- insertion order is kept
- element access via index
- are iterable
- size adjusts dynamically
- duplicate values are allowed
- findong and deleting elements can require extra work

* splice is peformance intensive because once you remove an element, the remaining elements have to move to new positions or memory location