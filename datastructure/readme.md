## responsible to manage data
### examples
1. arrays => [1, 2, 3, 4, 5] 
2. objects => {name: "joshua", age: 28}
3. maps => new Map() map.set('a', 'b')
4. sets => new Set() set.add(1)

## Tasks
1. ordered lists (duplicate allowed) memory mastered
    [1, 2, 3, 5, 4]
2. unordered lists (no duplicates allowed)
    new Set() set.add("pizza")
3. key value pairs or unordered data
    { name: "max", age: 28 }
4. key value pairs of ordered, iterable data
    new Map() map.set("loc", "germany")