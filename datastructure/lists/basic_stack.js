class Stack {
    constructor() {
        this.items = [];
    }

    push(value) {
        this.items.push(value);
    }

    pop() {
        return this.items.pop();
    }

    isEmpty() {
        return this.items.length === 0;
    }

    toArray() {
        return this.items.slice();
    }
}

const stack = new Stack();

stack.push('cook');
stack.push('wash');
stack.push('dish');
stack.push('ingredients');
console.log(stack);
stack.pop('ingredients');
stack.pop('dish');
console.log(stack);
