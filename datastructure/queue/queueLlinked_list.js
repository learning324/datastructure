const LinkedList  = require('../../datastructure/linkedLists/linked_lists')

class Queue {
    constructor() {
        this.list = new LinkedList();
    }

    enQueue(value) {
        this.list.append(value)
    }

    deQueue() {
        return this.list.deleteHead()
    }

    isEmpty() {
        return !this.list.head;
    }

    toArray() {
        return this.list.toArrary();
    }
}

const queue = new Queue();

queue.enQueue('max');
queue.enQueue('apoena');
queue.enQueue('joshua');
console.log(queue.toArray());
console.log(queue.deQueue());
console.log(queue.toArray());
