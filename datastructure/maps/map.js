const map = new Map();
let resOK = 0;
for (let i = 0; i < T.length; i++) {
  let last = T[i].charAt(T[i].length - 1);
  if (!Number(last)) last = T[i].charAt(T[i].length - 2);
  if (R[i] !== "OK") {
    map.set(last, "NOK");
  }
  if (!map.has(last) && R[i] === "OK") {
    map.set(last, "OK");
  }
  if (map.has(last) && R[i] !== "OK") {
    map.set(last, "NOK");
  }
}
for (let m of map) {
  if (m[1] === "OK") resOK++;
}
const res = Math.floor((resOK * 100) / map.size);
return res;
