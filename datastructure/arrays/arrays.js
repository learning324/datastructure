const names = ['Max', 'Manu', 'Julie', 'Max'];
// console.log(names.length);
console.log(names[1])
for( const el of names ) {
    console.log(el);
}

names.push('John')
console.log(names.length);

names.find(el => el === 'Max')
const findIndex = names.findIndex(el => el === 'Max');
console.log(findIndex);

names.splice(2, 2); //modify and remove elements
console.log(names)