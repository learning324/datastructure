## Theory behind datastructure

data structure os organizing data in a computer so that it can be used effectively

### Primitive Data structure

- The primitive data structures are primitive data types. The int, char, float, double, and pointer are the primitive data structures that can hold a single value.

### Non-Primitive Data structure

-The non-primitive data structure is divided into two types:

-Linear data structure
-Non-linear data structure


1. Searching - We can search for any element in a data structure.
2. Sorting - We can sort the elements of a data structure either in an ascending or descending order.
2. Insertion - We can also insert the new element in a data structure.
2. Updation - We can also update the element, i.e., we can replace the element with another element.
2. Deletion - We can also perform the delete operation to remove the element from the data structure.