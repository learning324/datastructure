const LinkedList  = require('../../datastructure/linkedLists/linked_lists')

class Stack {
    constructor() {
        this.list = new LinkedList();
    }

    push(value){
        this.list.prepend(value)
    }

    pop(value){
        return this.list.deleteHead(value);
    }

    isEmpty() {
        return this.list.head;
    }

    isArray() {
        return this.list.toArrary();
    }
}
module.exports = Stack;

const stack = new Stack();

stack.push('cook dinner');
stack.push('wash the dishes');
stack.push('buy ingredients');
console.log(stack.isArray());
stack.pop('buy ingredients');
stack.pop('wash the dishes');
console.log(stack);
