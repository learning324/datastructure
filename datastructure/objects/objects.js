const person = {
    firstName: 'Max',
    age: 31,
    hobbies: [ 'sports', 'cooking' ],
    greeting() {
        console.log('Hi, I am ' + this.firstName)
    },
}

for (const el in person) {
    console.log(el);
}

console.log(person['firstName']);
person.lastName = 'Isaac';
console.log(person);

person.greeting();

delete person.firstName;
console.log(person);
